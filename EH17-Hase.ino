#include <FastLED.h>

#define LED_PIN     5

// Information about the LED strip itself
#define NUM_LEDS    940
#define NUM_PATCHES 18
#define CHIPSET     WS2811
#define COLOR_ORDER GRB
#define BRIGHTNESS  32
#define FADE_STEP   2

CRGB leds[NUM_LEDS];

unsigned int framecounter = 0xFFFFFFFF;
static short patchends[NUM_PATCHES] = {80, 120, 170, 245, 295, 381, 393, 413, 425, 446, 712, 758, 846, 876, 894, 901, 924, 939};

char neigbours[NUM_PATCHES][8] = {
  { 1, 10, -1, -1, -1, -1, -1, -1}, // patch_IDs of patches neighboring patch_ID 0, -1 denotes the end of the list
  { 0,  3, 10, -1, -1, -1, -1, -1}, // patch_IDs of patches neighboring patch_ID 1
  { 3,  4,  5, -1, -1, -1, -1, -1}, // ...
  { 1,  2,  4, 10, -1, -1, -1, -1},
  { 2,  3,  5,  7,  8,  9, 10, -1},
  { 2,  4,  6,  7, -1, -1, -1, -1},
  { 5,  7, -1, -1, -1, -1, -1, -1},
  { 4,  5,  6,  8, -1, -1, -1, -1},
  { 4,  7,  9, -1, -1, -1, -1, -1},
  { 4,  8, 10, -1, -1, -1, -1, -1},
  { 0,  1,  3,  4,  9, 11, 14, -1},
  {10, 12, 13, 14, -1, -1, -1, -1},
  {11, 13, -1, -1, -1, -1, -1, -1},
  {11, 12, 14, 16, 17, -1, -1, -1},
  {10, 11, 13, 15, 16, -1, -1, -1},
  {14, 16, -1, -1, -1, -1, -1, -1},
  {13, 14, 15, 17, -1, -1, -1, -1},
  {13, 16, -1, -1, -1, -1, -1, -1}
};

// list of available colors
CRGB Colors[] = {CRGB::Purple, CRGB::White, CRGB::Blue, CRGB::Green, CRGB::Yellow, CRGB::Violet, CRGB::Red, CRGB::Green, CRGB::Orange};

// actual colors of the patches, required for fading
CRGB patch_colors[NUM_PATCHES] = {
  CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, 
  CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, 
  CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, CRGB::Black, 
  CRGB::Black, CRGB::Black, CRGB::Black
  };
char is_fading[NUM_PATCHES] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// color ID assignment to the several patches
unsigned char patch_color_IDs[] = {0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2};

// check wether patch_ID may be switched to color_ID - if so, do it
void set_color(unsigned char patch_ID, unsigned int color_ID) {
  if (is_fading[patch_ID])
    return;

  for (unsigned char neigbour_ID=0; neigbour_ID<8; neigbour_ID++) {
    char neighboring_patch_ID = neigbours[patch_ID][neigbour_ID];

    if (color_ID == patch_color_IDs[neighboring_patch_ID])
      return;

    if (neighboring_patch_ID == (char) -1) {
      patch_color_IDs[patch_ID] = color_ID;
      is_fading[patch_ID] = 1;
      return;
    }
  }
}


void setup() {
  delay( 3000 ); // power-up safety delay
  // It's important to set the color correction for your LED strip here,
  // so that colors can be more accurately rendered through the 'temperature' profiles
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalSMD5050 );
  FastLED.setBrightness(BRIGHTNESS);

  Serial.begin(9600);
}

void fade(char max_step) {
  // fade actual colors to the target colors
  for (char patch_ID = 0; patch_ID < NUM_PATCHES; patch_ID ++){
    if (true){//(!is_fading[patch_ID]) {
      int16_t act_r = (int16_t) patch_colors[patch_ID].r;
      int16_t act_g = (int16_t) patch_colors[patch_ID].g;
      int16_t act_b = (int16_t) patch_colors[patch_ID].b;

      int16_t end_r = (int16_t) Colors[patch_color_IDs[patch_ID]].r;
      int16_t end_g = (int16_t) Colors[patch_color_IDs[patch_ID]].g;
      int16_t end_b = (int16_t) Colors[patch_color_IDs[patch_ID]].b;

      act_r += max(-max_step, min(max_step, end_r - act_r));
      act_g += max(-max_step, min(max_step, end_g - act_g));
      act_b += max(-max_step, min(max_step, end_b - act_b));

      patch_colors[patch_ID] = CRGB(act_r, act_g, act_b);
      is_fading[patch_ID] = ((end_r == act_r) && (end_g == act_g) && (end_b == act_b)) ? 0 : 1;
    }
  }  
}

void loop() { 
  framecounter++;

  // every n frames, set random patch to random color  
  if(framecounter % 10 == 0) set_color(random(18), random(8));

  fade(FADE_STEP);

  // set all LEDs to the color for the respect patch
  unsigned char patch_ID = 0;
  for(short i=0; i<NUM_LEDS; i++) {
    if (patchends[patch_ID] <= i) patch_ID++;
//    leds[i] = Colors[patch_color_IDs[patch_ID]];
      leds[i] = patch_colors[patch_ID];
  } // */

  FastLED.show();
}

